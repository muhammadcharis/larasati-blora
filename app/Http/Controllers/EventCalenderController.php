<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\EventCalender;

class EventCalenderController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('event_calender.index');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'event_time' => 'required',
            'event_name' => 'required'
        ]);

        $id             = $request->id;
        $event_name     = strtolower(trim($request->event_name));
        $selected_date  = $request->selected_date;
        $event_time     = $request->event_time.':00';

        if($id)
        {

        }else
        {
            if(EventCalender::where([
                ['event_name',$event_name],
                [db::raw("to_char(event_date,'yyyy-mm-dd')"),$selected_date],
            ])
                ->exists()) return response()->json(['message' => 'Event name already exists.'], 422);

            try
            {
                DB::beginTransaction();
                
                EventCalender::firstorCreate([
                    'event_name'    => $event_name,
                    'event_date'    => $selected_date.' '.$event_time
                ]);
                
                DB::commit();
                $request->session()->flash('message', 'success');
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json('success', 200);
        }
    }

    public function data()
    {
        /*
            var events = [
                {
                    title: 'Long Event',
                    start: '2019-10-13',
                    end: '2019-10-13',
                    color: '#FFCCBC',
                    rendering: 'background'
                }
            ];
        */

        $event_calenders = EventCalender::select(db::raw("to_char(event_date,'yyyy-mm-dd') as event_date"))
        ->groupby(db::raw("to_char(event_date,'yyyy-mm-dd')"))
        ->get();

        $array = [];
        foreach ($event_calenders as $key => $event_calender) 
        {
            $obj            = new stdclass();
            $obj->start     = $event_calender->event_date->format('Y-m-d');
            $obj->end       = $event_calender->event_date->format('Y-m-d');
            $obj->color     = '#5ffa88';
            $obj->rendering = 'background';
            $array [] = $obj;
        }

        return response()->json($array,200);
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax()) 
        {
            $selected_date  = $request->selected_date;
            
            
            $details        = EventCalender::where(db::raw("to_char(event_date,'yyyy-mm-dd')"),$selected_date)
            ->orderby('event_date','asc');
            
            return DataTables::of($details)
            ->editColumn('event_date',function ($details)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $details->event_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('total_yard',function ($details)
            {
                return ucwords($details->event_name);
            })
            ->addColumn('action', function($data) {
                return view('event_calender._action', [
                    'model' => $data,
                    'delete' => route('eventCalender.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function destroy($id)
    {
        EventCalender::find($id)->delete();
        return response()->json('success', 200);
    }
}
