<?php namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Mail;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Role;
use App\Models\Temporary;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg        = $request->session()->get('message');
        return view('user.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = User::whereNull('deleted_at')->orderby('created_at','desc');

            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('sex',function($data){
            	return ucwords($data->sex);
            })
            ->addColumn('action', function($data) {
                return view('user._action', [
                    'model' => $data,
                    'edit' => route('user.edit',$data->id),
                    'delete' => route('user.destroy',$data->id),
                    'reset' => route('user.resetPassword',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $roles = Role::pluck('name', 'id')->all();
        return view('user.create',compact('roles'));
    }

    public function store(Request $request)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);

        if(User::where('email',$request->email)->exists()) 
                return response()->json('Email already exists.', 422);

        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        try
        {
            DB::beginTransaction();
            
            $user = User::firstorCreate([
                'name'              => strtolower($request->name),
                'sex'               => $request->sex,
                'email'             => $request->email,
                'email_verified_at' => carbon::now(),
                'password'          => bcrypt('password1'),
            ]);
            

            $mappings =  json_decode($request->mappings);
            $array = array();

            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
            
            if($user->save())
            {
                /*if($user->is_supplier)
                {
                    //send confirmation email
                    Mail::send('email.account_confirmation', ['user' => $user], function ($message) use ($user)
                    {
                        $message->subject('[WPO] Confirmation Account');
                        $message->from('no-reply@wpo.bbigroup.id', 'WPO');
                        $message->to($user->email);
                    });
                }*/
                $user->attachRoles($array);
            } 
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function resendActivation(Request $request,$id)
    {
        $user = User::find($id);
        if($user->email_verified_at) return response()->json(['message' => 'This user already active'],422);

        if($user->is_supplier)
        {
            //send confirmation email
            Mail::send('email.account_confirmation', ['user' => $user], function ($message) use ($user)
            {
                $message->subject('[WPO] Confirmation Account');
                $message->from('no-reply@wpo.bbigroup.id', 'WPO');
                $message->to($user->email);
            });

            return response()->json('success',200);
        }
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $user           = User::find($id);
        $type           = ($user->is_supplier)? 'external' : 'internal';
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role) 
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        $roles      = Role::pluck('display_name', 'id')->all();
       
        return view('user.edit',compact('roles','user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);
        
        if(User::where([
            ['email',$request->email],
            ['id','!=',$id],
        ])->exists()) 
            return response()->json('Email already exists.', 422);

        $user = User::find($id);
        $image = null;

        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                if(File::exists($avatar)) File::delete($old_file);

                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $user->name     = strtolower($request->name);
        $user->email    = $request->email;
        $user->sex      = $request->sex;
        if ($image) $user->photo = $image;
        $user->save();

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->deleted_at = carbon::now();
        $user->deleted_user_id = auth::user()->id;
        $user->save();
        return response()->json(200);
    }

    public function resetPassword($id)
    {
        $user           = User::findorFail($id);

        if($user->is_supplier)
        {
            Temporary::FirstOrCreate([
                'column_1' => 'send_reset_password',
                'column_2' =>  $user->email,
            ]);
        }else
        {
            $user->password = bcrypt('password1');
            $user->save();
        }
       
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        if($user->is_supplier)
        {
            return response()->json(['message' => 'kelompok akses tidak dapat dihapus'],422); 
        }
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }

    public function getInformationUser(Request $request)
    {
        $nik                = $request->nik;
        $user_information   = DB::connection('absence_bbi')
            ->table('get_employee') 
            ->where('nik',$nik)
            ->first();

        return response()->json($user_information,200);
    }
}
