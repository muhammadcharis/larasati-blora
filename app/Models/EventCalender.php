<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class EventCalender extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['event_date','event_name'];
    protected $dates     = ['event_date'];

}
