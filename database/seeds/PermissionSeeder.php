<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-event-calender',
                'display_name' => 'menu event calender',
                'description' => 'menu event calender'
            ],
            [
                'name' => 'menu-invoicing',
                'display_name' => 'menu invoicing',
                'description' => 'menu invoicing'
            ],
            
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
