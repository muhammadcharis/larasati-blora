<?php use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $permissions = Permission::select('id')->get();
        $role = new Role();
        $role->name = 'admin_larasati';
        $role->display_name = 'Admin Larasati';
        $role->description  = 'super admin larasati';

        
        if($role->save()){
            $role->attachPermissions($permissions);
        }
    }
}
