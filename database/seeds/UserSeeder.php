<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Admin Larasati Blora';
        $user->sex = 'laki';
        $user->email = 'admin@larasati-blora.com';
        $user->email_verified_at = carbon::now();
        $user->password =  bcrypt('password1');

        if($user->save())
            $user->attachRoles($roles);   
    }
}
