$(function() 
{
    var today = $('#today').val();

    $("#event_time").AnyTime_picker({
        format: "%H:%i"
    });
    
    $('.fullcalendar-basic').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title'
        },
        defaultDate: today,
        editable: true,
        events: '/admin/event-calender/data',
        dayClick: function(date, jsEvent, view) 
        {
            $('#_selected_date').text(date.format('D')+'/'+date.format('M')+'/'+date.format('Y'));
            $('#selected_date').val(date.format());
            $('#detailModal').modal(); 
            var url_detail_data = $('#url_detail_data').val();
            
            var detailTable = $('#detail_table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength:10,
                scrollY:250,
                scroller:true,
                destroy:true,
                deferRender:true,
                bFilter:true,
                ajax: {
                    type: 'GET',
                    url: url_detail_data,
                    data: function(d) {
                        return $.extend({}, d, {
                            "selected_date" : $('#selected_date').val(),
                        });
                   }
                },
                fnCreatedRow: function (row, data, index) {
                    var info = detailTable.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'event_date', name: 'event_date',searchable:true,visible:true,orderable:true,width:'50%'},
                    {data: 'event_name', name: 'event_name',searchable:true,visible:true,orderable:true,width:'50%'},
                    {data: 'action', name: 'action',searchable:false,visible:true,orderable:false}
                ]
            });

            var dtable = $('#detail_table').dataTable().api();
            $(".dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
            });
        },
    });


    $('#form').submit(function (event){
        event.preventDefault();
        var event_time = $('#event_time').val();
        var event_name = $('#event_name').val();
        
        if(!event_time)
        {
            $("#alert_warning").trigger("click", 'Please insert time first');
            return false
        }

        if(!event_name)
        {
            $("#alert_warning").trigger("click", 'Please insert name first');
            return false
        }
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () 
                    {
                        
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            },
                            baseZ: 2000
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () 
                    {
                        $('#event_time').val('');
                        $('#event_name').val('');
                        $("#alert_success").trigger("click",'Data successfully saved');
                        $('#detail_table').DataTable().ajax.reload();
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    
                    }
                });
            }
        });
    });
});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait',
                    baseZ: 2000
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#detail_table').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}
