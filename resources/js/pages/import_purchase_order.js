$(document).ready( function ()
{
    result = JSON.parse($('#result').val());
});

$('#select_supplier').on('change',function () 
{
    $('#form_download').submit();
    
});

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
    //$('#upload_file_allocation').submit();
    $.ajax({
        type: "post",
        url: $('#upload_file_allocation').attr('action'),
        data: new FormData(document.getElementById("upload_file_allocation")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Upload successfully.');
            result = [];
            for(idx in response)
            {
                var input = response[idx];
                result.push(input);
            }
            
        },
        error: function (response) {
            $.unblockUI();
            $('#upload_file_allocation').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

            if (response['status'] == 422)
                $("#alert_error").trigger("click", response.responseJSON.message);

        }
    })
    .done(function () {
        $('#upload_file_allocation').trigger('reset');
        render();
    });

})

function render() 
{
    getIndex();
    $('#result').val(JSON.stringify(result));
    var tmpl = $('#upload').html();
    Mustache.parse(tmpl);
    var data = { item: result };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
}

function getIndex() 
{
    for (idx in result) 
    {
        result[idx]['_id'] = idx;
        result[idx]['no'] = parseInt(idx) + 1;
    }
}

