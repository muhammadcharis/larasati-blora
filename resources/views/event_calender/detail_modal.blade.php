<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h5 class="modal-title">Detail Date <span id="_selected_date"></span></h5>
			</div>
				
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							{!!
								Form::open([
									'role'	 	=> 'form',
									'url'	 	=> route('eventCalender.store'),
									'method' 	=> 'post',
									'class' 	=> 'form-horizontal',
									'id'		=> 'form'
								])
							!!}
							
								@include('form.text', [
									'field' 			=> 'event_time',
									'label' 			=> 'Event Time',
									'label_col' 		=> 'col-lg-4 col-md-8 col-sm-12',
									'form_col' 			=> 'col-lg-8 col-md-8 col-sm-12',
									'attributes' 		=> [
										'id' 			=> 'event_time',
										'autocomplete' 	=> 'off'
									]
								])
								
								@include('form.text', [
									'field' 			=> 'event_name',
									'label' 			=> 'Event Name',
									'label_col' 		=> 'col-lg-4 col-md-8 col-sm-12',
									'form_col' 			=> 'col-lg-8 col-md-8 col-sm-12',
									'attributes' 		=> [
										'id' 			=> 'event_name',
										'autocomplete' 	=> 'off'
									]
								])
							{!! Form::hidden('id',null, array('id' => 'id')) !!}
							{!! Form::hidden('selected_date',null, array('id' => 'selected_date')) !!}
							<button type="submit" class="btn btn-primary col-xs-12">Save <i class="icon-floppy-disk position-left"></i></button>
							{!! Form::close() !!}
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="table-responsive">
									<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_table">
										<thead>
											<tr>
												<th>No</th>
												<th width="50%">Event Date</th>
												<th width="50%">Event Name</th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				
				{!! Form::hidden('url_detail_data', route('eventCalender.dataDetail'), array('id' => 'url_detail_data')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
			
		</div>
	</div>
</div>
