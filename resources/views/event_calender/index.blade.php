@extends('layouts.app',['active' => 'event_calender'])

@section('page-css')
    <style>
        div.AnyTime-win {z-index:9999}
    </style>
@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Event Calender</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Event Calender</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="fullcalendar-basic"></div>
        </div>
    </div>

    {!! Form::hidden('today', \Carbon\Carbon::now()->format('Y-m-d'), array('id' => 'today')) !!}
@endsection

@section('page-modal')
    @include('event_calender.detail_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/event_calender.js') }}"></script>
@endsection
