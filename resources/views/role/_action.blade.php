
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{!! $edit_modal !!}">Edit <i class="icon-pencil6 pull-right"></i></a></li>    
            <li><a href="#" onclick="hapus('{!! $delete !!}')">Delete <i class="icon-trash pull-right"></i></a></li>
        </ul>
    </li>
</ul>
