
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{!! $edit !!}" ><i class="icon-pencil6 pull-right"></i> Edit</a></li>
            <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-trash pull-right"></i> Delete</a></li>
            <li><a href="#" onclick="reset('{!! $reset !!}')"><i class="icon-reset pull-right"></i> Reset Password</a></li>
            @if(isset($resend))
                <li><a href="#" onclick="resend('{!! $resend !!}')"><i class="icon-mail5 pull-right"></i> Resend Activation</a></li>
            @endif
        </ul>
    </li>
</ul>
