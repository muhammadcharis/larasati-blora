@extends('layouts.app',['active' => 'user'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Account Management</li>
            <li><a href="{{ route('user.index') }}">User</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-body">
                {!!
                    Form::open([
                        'role' => 'form',
                        'url' => route('user.update',$user->id),
                        'method' => 'post',
                        'class' => 'form-horizontal',
                        'enctype' => 'multipart/form-data',
                        'id'=> 'form'
                    ])
                !!}
                    
                    @include('form.text', [
                        'field'         => 'name',
                        'label'         => 'Name',
                        'default'       => $user->name,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'name',
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'email',
                        'label'         => 'Email',
                        'default'       => $user->email,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'email',
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'sex',
                        'default'       => $user->sex,
                        'label'         => 'Sex',
                        'mandatory'     => '*Require',
                        'options'       => [
                            ''          => '-- Select Sex --',
                            'laki'      => 'Male',
                            'perempuan' => 'Female',
                        ],
                        'class'         => 'select-search',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'select_kelamin'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'role',
                        'label'         => 'Role',
                        'options'       => [
                            ''          => '-- Select Role --',
                        ]+$roles,
                        'class'         => 'select-search',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'select_role'
                        ]
                    ])
                    
                    @include('form.file', [
                        'field'         => 'photo',
                        'label'         => 'Upload Photo',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'help'          => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
                        'attributes'    => [
                            'id'        => 'photo',
                            'accept'    => 'image/*'
                            ]
                        ])
                        
                       
                    {!! Form::hidden('user_id', $user->id, array('id' => 'user_id')) !!}
                    {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
                    {!! Form::hidden('mappings', json_encode($mappings), array('id' => 'mappings')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold">List Role</span></h6>
            </div>
            <div class="panel-body">
                <table class="table table-basic table-striped table-hover table-condensed" id="roleTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('url_data_role',route('user.dataRole',$user->id), array('id' => 'url_data_role')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/switch.js') }}"></script>
<script src="{{ mix('js/user.js') }}"></script>
@endsection
