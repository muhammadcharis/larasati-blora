@extends('layouts.app',['active' => 'user'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">User</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('admin.home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li>Account Management</li>
                <li class="active">User</li>
            </ul>
            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('user.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-default border-grey">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-striped table-hover table-condensed" id="userTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th width="40%">Name</th>
                            <th width="40%">Email</th>
                            <th width="40%">Sex</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/user.js') }}"></script>
@endsection
