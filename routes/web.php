<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return 'front-end';
});

Route::prefix('admin')->group(function()
{
    Route::get('', 'HomeController@index')->name('admin.home');
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.forgot_password');
    Route::get('password/reset/{id}/{date}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.resetpassword');

    Route::post('login', 'Auth\LoginController@login')->name('admin.do_login');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.do_logout');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.sendlink');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.reset');

    Route::get('account-setting', 'HomeController@accountSetting')->name('accountSetting');
    Route::put('account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
    Route::get('account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');


    Route::prefix('event-calender')->middleware(['permission:menu-event-calender'])->group(function()
    {
        Route::get('', 'EventCalenderController@index')->name('eventCalender.index');
        Route::get('create', 'EventCalenderController@create')->name('eventCalender.create');
        Route::get('data', 'EventCalenderController@data')->name('eventCalender.data');
        Route::get('data-detail', 'EventCalenderController@dataDetail')->name('eventCalender.dataDetail');
        Route::post('store', 'EventCalenderController@store')->name('eventCalender.store');
        Route::delete('delete/{id}', 'EventCalenderController@destroy')->name('eventCalender.destroy');
    });
    
    Route::prefix('account-management')->group(function()
    {
        Route::prefix('permission')->middleware(['permission:menu-permission'])->group(function()
        {
            Route::get('', 'PermissionController@index')->name('permission.index');
            Route::get('create', 'PermissionController@create')->name('permission.create');
            Route::get('data', 'PermissionController@data')->name('permission.data');
            Route::post('store', 'PermissionController@store')->name('permission.store');
            Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
            Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
            Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
        });

        Route::prefix('role')->middleware(['permission:menu-role'])->group(function()
        {
            Route::get('', 'RoleController@index')->name('role.index');
            Route::get('create', 'RoleController@create')->name('role.create');
            Route::get('data', 'RoleController@data')->name('role.data');
            Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
            Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
            Route::post('store', 'RoleController@store')->name('role.store');
            Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
            Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
            Route::post('update/{id}', 'RoleController@update')->name('role.update');
            Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
        });

        Route::prefix('user')->middleware(['permission:menu-user'])->group(function()
        {
            Route::get('', 'UserController@index')->name('user.index');
            Route::get('create', 'UserController@create')->name('user.create');
            Route::get('data', 'UserController@data')->name('user.data');
            Route::get('get-information-user', 'UserController@getInformationUser')->name('user.getInformationUser');
            Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
            Route::get('update/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
            Route::post('store', 'UserController@store')->name('user.store');
            Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
            Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
            Route::post('update/{id}', 'UserController@update')->name('user.update');
            Route::post('resend-activation/{id}', 'UserController@resendActivation')->name('user.resendActivation');
            Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
            Route::put('delete/{id}', 'UserController@destroy')->name('user.destroy');
        });
    });
});

